import { HttpParams } from '@angular/common/http';

export const createRequestOptions = (req?: any): HttpParams => {
    let options: HttpParams = new HttpParams();
    if (req) {
      Object.keys(req).forEach(key => {
        options = options.set(key, req[key]);
      });
    }
  
    return options;
};