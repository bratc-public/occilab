import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { BussinesDataComponent } from './bussines-data/bussines-data.component';
import { CreditLineComponent } from './credit-line/credit-line.component';
import { FooterComponent } from './footer/footer.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCoffee, faArrowLeft, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { MainComponent } from './main/main.component';



@NgModule({
  declarations: [NavbarComponent, BussinesDataComponent, CreditLineComponent, FooterComponent, MainComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  exports: [
    MainComponent,
    NavbarComponent
  ]
})
export class OcciCreditModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCoffee);
    library.addIcons(faArrowLeft);
    library.addIcons(faInfoCircle);
  }
 }
