import { Component, OnInit } from '@angular/core';
import { IClient } from '../client.model';
import { OcciCreditService } from '../occi-credit.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-bussines-data',
  templateUrl: './bussines-data.component.html',
  styleUrls: ['./bussines-data.component.scss']
})
export class BussinesDataComponent implements OnInit {
  client?: IClient;
  constructor(private occicreditService: OcciCreditService) { }

  
  ngOnInit(): void {
      this.getAllData();
  }
  
  /**
   * Method get all from service and send param query String 
   */
  getAllData(): void {
    this.occicreditService.query({
      nit: "800220154"
    })
    .subscribe((res: HttpResponse<IClient>) => {
      this.client = res.body;
    },
    () => {});
  }

}
