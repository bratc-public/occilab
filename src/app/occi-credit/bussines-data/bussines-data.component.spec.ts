import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BussinesDataComponent } from './bussines-data.component';
import { OcciCreditService } from '../occi-credit.service';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Client } from '../client.model';
import { of } from 'rxjs';
import { OcciCreditModule } from '../occi-credit.module';

describe('BussinesDataComponent', () => {
  let component: BussinesDataComponent;
  let fixture: ComponentFixture<BussinesDataComponent>;

  let service: OcciCreditService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BussinesDataComponent ],
      imports: [OcciCreditModule],
      providers: [ 
        OcciCreditService
      ]
    })
    .overrideTemplate(BussinesDataComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BussinesDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = fixture.debugElement.injector.get(OcciCreditService);
  });

  it('should call load all on init', () => {
      // GIVEN
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: new Client('800220154'),
          })
        )
      );
      
     // WHEN 
   component.ngOnInit();
    // Then
    expect(service.query).toHaveBeenCalled();
    expect(component.client).toEqual(jasmine.objectContaining({nit: "800220154"})); 
  }); // end it

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
