import { TestBed } from '@angular/core/testing';

import { OcciCreditService } from './occi-credit.service';

describe('OcciCreditService', () => {
  let service: OcciCreditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OcciCreditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
