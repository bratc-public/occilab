export interface IClient {
    nit?: string;
    nombre?: string;
    capa?: string;
    segmento?: string;
    gerenteRelacion?: string;

}

export class Client implements IClient {
    constructor(
       public nit?: string,
       public nombre?: string,
       public capa?: string,
       public segmento?: string,
       public gerenteRelacion?: string
    ){}
}