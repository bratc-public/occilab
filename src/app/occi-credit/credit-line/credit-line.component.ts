import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-credit-line',
  templateUrl: './credit-line.component.html',
  styleUrls: ['./credit-line.component.scss']
})
export class CreditLineComponent implements OnInit {
  operation: string = 'active';
  indicator: string;
  info: string;
  constructor() { }

  ngOnInit(): void {
  }

  selectedItem(item: string): void {
    if (item === 'o') {
      this.indicator = '';
      this.info = '';
      this.operation = 'active';
    } else if (item === 'i') {
      this.indicator = 'active';
      this.info = '';
      this.operation = '';
    } else if (item ==='f') {
      this.info = 'active';
      this.operation = '';
      this.indicator = '';
    }
  }
}
