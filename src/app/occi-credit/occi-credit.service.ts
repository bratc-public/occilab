import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IClient } from './client.model';
import { environment } from 'src/environments/environment';
import { createRequestOptions } from 'src/app/shared/request-options';
type EntityResponseType = HttpResponse<IClient>;
@Injectable({
  providedIn: 'root'
})
export class OcciCreditService {
  constructor(private http: HttpClient) { }
/**
 * Method to get all data from API
 * @param req 
 */
  public query(req?: any): Observable<EntityResponseType> {
    const options = createRequestOptions(req);
    return this.http.get<IClient>(`${environment.ENDPOIN}/occilabFunction`, {params: options, observe: 'response'});
  }

}
