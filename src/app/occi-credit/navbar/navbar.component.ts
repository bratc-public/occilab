import { Component, OnInit } from '@angular/core';
import { IClient } from '../client.model';
import { OcciCreditService } from '../occi-credit.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  client?: IClient;
  constructor(private occicreditService: OcciCreditService) { }

  ngOnInit(): void {  
    this.getAllData();  
  }
 /**
   * Method get all from service and send param query String 
   */
  getAllData(): void {
    this.occicreditService.query({
      nit: "800220154"
    })
    .subscribe((res: HttpResponse<IClient>) => {
      this.client = res.body;
    },
    () => {});

  } 
} 
