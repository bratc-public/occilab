import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OcciCreditModule } from './occi-credit/occi-credit.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OcciCreditModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
